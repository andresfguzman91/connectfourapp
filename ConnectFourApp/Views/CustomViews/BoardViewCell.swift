//
//  BoardViewCell.swift
//  ConnectFourApp
//
//  Created by Andrés Guzmán on 2/12/19.
//  Copyright © 2019 Andres Felipe Guzman Lopez. All rights reserved.
//

import UIKit

class BoardViewCell: UICollectionViewCell {
    @IBOutlet weak var tokenImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
