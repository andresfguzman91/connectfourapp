//
//  AppConstants.swift
//  ConnectFourApp
//
//  Created by Andrés Guzmán on 2/12/19.
//  Copyright © 2019 Andres Felipe Guzman Lopez. All rights reserved.
//

import Foundation

struct AppConstants {
    static let reusableCellID = "boardCell"
    static let reusableGameCellID = "gameCellID"
}
